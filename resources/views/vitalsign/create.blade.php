@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card card-info mt-3">
		<div class="card-header">Vital Sign - {{ $patient->firstname }} {{ $patient->lastname }}</div>
		<div class="card-body">
			<form method="POST" action="{{ route('vitalsign.store',['id' => $id]) }}">
				@include('shared.alerts')
				<div class="row">
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="height">Height:</label>
					    <input type="text" class="form-control" id="height" name="height" value="{{ old('height') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="weight">Weight:</label>
					    <input type="text" class="form-control" id="weight" name="weight" value="{{ old('weight') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="bp">BP:</label>
					    <input type="text" class="form-control" id="bp" name="bp" value="{{ old('bp') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="pulse">Pulse:</label>
					    <input type="text" class="form-control" id="pulse" name="pulse" value="{{ old('pulse') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="respiration">Respiration:</label>
					    <input type="text" class="form-control" id="respiration" name="respiration" value="{{ old('respiration') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="pulse">BMI:</label>
					    <input type="text" class="form-control" id="bmi" name="bmi" value="{{ old('bmi') }}">
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="ppd">PPD:</label>
					    <input type="text" class="form-control" id="ppd" name="ppd" value="{{ old('ppd') }}">
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="mmr">MMR:</label>
					    <input type="text" class="form-control" id="mmr" name="mmr" value="{{ old('mmr') }}">
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="bodybuilt">Body Built:</label>
					    <input type="text" class="form-control" id="bodybuilt" name="bodybuilt" value="{{ old('bodybuilt') }}">
					</div>
					<div class="form-group col-12 col-sm-4">
					    <label class="form-control-label" for="lmp">L.M.P:</label>
					    <input type="text" class="form-control" id="lmp" name="lmp" value="{{ old('lmp') }}">
					</div>
					<div class="form-group col-12 col-sm-4">
					    <label class="form-control-label" for="lsc">L.S.C:</label>
					    <input type="text" class="form-control" id="lsc" name="lsc" value="{{ old('lsc') }}">
					</div>
					<div class="form-group col-12 col-sm-4">
					    <label class="form-control-label" for="method">Method:</label>
					    <input type="text" class="form-control" id="method" name="method" value="{{ old('method') }}">
					</div>
					<div class="form-group col-12 col-sm-2">
						{{ csrf_field() }}
					    <button class="btn btn-info">Save</button>
						
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection