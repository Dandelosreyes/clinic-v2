@extends('layouts.app')
@section('content')
<div class="container">
		<div class="card card-info mt-3">
		<div class="card-header">Updating Vital Sign - {{ $patient->firstname }} {{ $patient->lastname }}</div>
		<div class="card-body">
			<form method="POST" action="{{ route('vitalsign.update',['id' => $vitals->id]) }}">

				{{csrf_field()}} {{ method_field('patch') }}
				@include('shared.alerts')
				<div class="row">
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="height">Height:</label>
					    <input type="text" class="form-control" id="height" name="height" value="{{ $vitals->height }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="weight">Weight:</label>
					    <input type="text" class="form-control" id="weight" name="weight" value="{{ $vitals->weight }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="bp">BP:</label>
					    <input type="text" class="form-control" id="bp" name="bp" value="{{ $vitals->bp }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="pulse">Pulse:</label>
					    <input type="text" class="form-control" id="pulse" name="pulse" value="{{ $vitals->pulse }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="respiration">Respiration:</label>
					    <input type="text" class="form-control" id="respiration" name="respiration" value="{{ $vitals->respiration }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="pulse">BMI:</label>
					    <input type="text" class="form-control" id="bmi" name="bmi" value="{{ $vitals->bmi }}">
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="ppd">PPD:</label>
					    <input type="text" class="form-control" id="ppd" name="ppd" value="{{ $vitals->ppd }}">
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="mmr">MMR:</label>
					    <input type="text" class="form-control" id="mmr" name="mmr" value="{{ $vitals->mmr }}">
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="bodybuilt">Body Built:</label>
					    <input type="text" class="form-control" id="bodybuilt" name="bodybuilt" value="{{ $vitals->bodybuilt }}">
					</div>
					<div class="form-group col-12 col-sm-4">
					    <label class="form-control-label" for="lmp">L.M.P:</label>
					    <input type="text" class="form-control" id="lmp" name="lmp" value="{{ $vitals->lmp }}">
					</div>
					<div class="form-group col-12 col-sm-4">
					    <label class="form-control-label" for="lsc">L.S.C:</label>
					    <input type="text" class="form-control" id="lsc" name="lsc" value="{{ $vitals->lsc }}">
					</div>
					<div class="form-group col-12 col-sm-4">
					    <label class="form-control-label" for="method">Method:</label>
					    <input type="text" class="form-control" id="method" name="method" value="{{ $vitals->method }}">
					</div>
				</div>	
				<div class="row">
					<div class="form-group col-12 col-sm-2">
						{{ csrf_field() }}
					    <button class="btn btn-info">Save</button>
						
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection