@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-primary mt-4 ">
	<div class="card-header">
	    Patient Information
	    <a href="{{ route('information.index') }}" class="btn btn-info btn-sm float-right">Back</a>
    </div>
	<div class="card-body">
		<form class="ml-3 mr-3"  method="POST" action="{{ route('information.store') }}">
			{{ csrf_field() }}
			@include('shared.alerts')
			<div class="row mt-3">
				<div class="form-group col-12 col-sm-4">
				    <label class="form-control-label" for="lastname">Lastname:</label>
				    <input type="text" class="form-control" id="lastname" name="lastname" value="{{ old('lastname') }}" placeholder="Dela Cruz">
				</div>
				<div class="form-group col-12 col-sm-4">
					<label class="form-control-label" for="firstname">Firstname:</label>
				    <input type="text" class="form-control" id="firstname" name="firstname" value="{{ old('firstname') }}" placeholder="Juan"> 
				</div>
				<div class="form-group col-12 col-sm-4">
					<label class="form-control-label" for="middlename">Middlename:</label>
				    <input type="text" class="form-control" id="middlename" name="middlename" value="{{ old('middlename') }}" placeholder="Basmayor">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-12 col-sm-12">
					<label class="form-control-label" for="mailingaddress">Mailing Address:</label>
					<input type="text" class="form-control" id="mailingaddress" name="mailingaddress" value="{{ old('mailingaddress') }}" placeholder="Vista Verde Executive Village, Cainta Rizal">
				</div>
			</div>	
			<div class="row">
				<div class="form-group col-12 col-sm-12 col-lg-2">
				    <label class="form-control-label" for="passport">Passport:</label>
				    <input type="text" class="form-control" id="passport" name="passport" value="{{ old('passport') }}"  placeholder="E03BX44">
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-2">
				    <label class="form-control-label" for="gender">Gender:</label>
				    <select id="gender" name="gender" class="form-control">
				    	<option value="1" @if(old('gender') == 1) selected @endif>Male</option>
				    	<option value="2" @if(old('gender') == 2) selected @endif>Female</option>
				    </select>
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-3">
				    <label class="form-control-label" for="birthday">Birthday:</label> 
				    <input type="date" class="form-control" id="birthday" name="birthday" value="{{ old('birthday') }}" placeholder="dd/mm/yy">
				</div>
				<div class="form-group col-12 col-sm12 col-lg-5">
				    <label class="form-control-label" for="placeofbirth">Place of birth:</label> 
				    <input type="text" class="form-control" id="placeofbirth" name="placeofbirth" value="{{ old('placeofbirth') }}" placeholder="Meycauayan, Bulacan">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-12 col-sm-12 col-lg-2">
					<label class="form-control-label" class="form-control">Civil Status:</label>
					<select id="civilstatus" name="civilstatus" class="form-control">
						<option value="1" @if(old('civilstatus') == 1) selected @endif>Single</option>
						<option value="2" @if(old('civilstatus') == 2) selected @endif>Married</option>
						<option value="3" @if(old('civilstatus') == 3) selected @endif>Divorced</option>
						<option value="4" @if(old('civilstatus') == 4) selected @endif>Widow</option>
					</select>
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-4">
					<label class="form-control-label" for="phonenum">Cellphone Number:</label> 
				    <input type="text" class="form-control" id="phonenum" name="phonenum" value="{{ old('phonenum') }}" placeholder="">
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-3">
					<label class="form-control-label" for="occupation">Occupation:</label> 
				    <input type="text" class="form-control" id="occupation" name="occupation" value="{{ old('occupation') }}" placeholder="HR Manager">
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-3">
					<label class="form-control-label" for="destination">Destination:</label> 
				    <select id="destination" name="destination" class="form-control">
						<option value="LOCAL" @if(old('destination') == "LOCAL") selected @endif>LOCAL</option>
						<option value="FOREIGN" @if(old('destination') == "FOREIGN") selected @endif>FOREIGN</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row">
				@foreach($diseases as $disease)
					<div class="col-12 col-sm-7 col-md-6">
						<input type="checkbox"  id="{{ $disease->id }}"  name="disease[{{$disease->id}}]" value="{{ $disease->id }}" {{ in_array($disease->id,old('disease',[])) ?'checked':'' }} >
						<label for="{{ $disease->id }}" class="control-label">{{ $disease->disease }}</label>
					</div>
				@endforeach
			</div>
			<hr>
			<div class="form-group row">
		      <div class="col-sm-10">
		        <button type="submit" class="btn btn-primary">Save</button>
		      </div>
		    </div>
		</form>
	</div>
</div>
</div>
@endsection