@extends('layouts.app')
@section('content')
<div class="container">
		<div class="card card-info mt-3">
		<div class="card-header">
		    Eye Sight of Patient {{ $vitals->information_id }}
		    <a href="{{ route('eyesight.index') }}" class="btn btn-sm btn-info float-right">Back</a>
		    </div>
		<div class="card-body">
			<form method="POST" action="{{ route('eyesight.update',['id' => $vitals->id]) }}">

				{{csrf_field()}} {{ method_field('patch') }}
				@include('shared.alerts')
				<div class="row">
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="uc_fv_od">Uncorrected Far Vision OD:</label>
					    <input type="text" class="form-control" id="uc_fv_od" name="uc_fv_od" value="{{ $vitals->uncorrected_fv_od }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="uc_fv_os">Uncorrected Far Vision OS:</label>
					    <input type="text" class="form-control" id="uc_fv_os" name="uc_fv_os" value="{{ $vitals->unccorected_fv_os }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="c_fv_od">Corrected Far Vision OD:</label>
					    <input type="text" class="form-control" id="c_fv_od" name="c_fv_od" value="{{ $vitals->corrected_fv_od }}">
					</div>
					{{-- {{ dd($vitals)}} --}}
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="c_fv_os">Corrected Far Vision OS:</label>
					    <input type="text" class="form-control" id="c_fv_os" name="c_fv_os" value="{{ $vitals->corrected_fv_os }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="uc_nv_od">Uncorrected Near Vision OD:</label>
					    <input type="text" class="form-control" id="uc_nv_od" name="uc_nv_od" value="{{ $vitals->uncorrected_nv_od }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="uc_nv_os">Uncorrected Near Vision OS:</label>
					    <input type="text" class="form-control" id="uc_nv_os" name="uc_nv_os" value="{{ $vitals->unccorected_nv_os }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="c_nv_od">Corrected Near Vision OD:</label>
					    <input type="text" class="form-control" id="c_nv_od" name="c_nv_od" value="{{ $vitals->corrected_nv_od }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="c_nv_os">Corrected Near Vision OS:</label>
					    <input type="text" class="form-control" id="c_nv_os" name="c_nv_os" value="{{ $vitals->corrected_nv_os}}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="optical">Optical:</label>
					   	<select class="form-control" name="optical" id="optical">
					   		<option value="0" @if($vitals->optical == 0) selected @endif>For Opto Clearance</option>
					   		<option value="1" @if($vitals->optical == 1) selected @endif>Cleared</option>
					   	</select>
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="colorvision">Color Vision:</label>
					    <select class="form-control" name="colorvision" id="colorvision">
					   		<option value="0" @if($vitals->colorvision == 0) selected @endif>Defective</option>
					   		<option value="1" @if($vitals->colorvision == 1) selected @endif>>Adequate</option>
					   	</select>
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="ad">Hearing AD:</label>
					    <input type="text" class="form-control" id="ad" name="ad" value="{{ $vitals->ad }}">
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="as">Hearing AS:</label>
					    <input type="text" class="form-control" id="as" name="as" value="{{ $vitals->as }}">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-12 col-sm-2">
						{{ csrf_field() }}
					    <button class="btn btn-info">Save</button>
						
				</div>
			</form>
		</div>
	</div>
</div>
@endsection