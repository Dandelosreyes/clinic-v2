@extends('layouts.app')
@section('content') 
<div class="container mt-4">
    <div class="card card-info">
        <div class="card-header">
             Patients
        </div>
        <div class="card-body">
            @include('shared.alerts')
            <table class="table table-light table-bordered" id="myTable">
                <thead>
                    <th>ID</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($patients as $patient)
                    <tr>
                        <td>{{ $patient->id }}</td>
                        <td>{{ $patient->firstname }}</td>
                        <td>{{ $patient->lastname }}</td>
                        <td>{{ $patient->created_at }}</td>
                        <td>
                            @if($patient->eyesight->isEmpty())
                            <a href="{{ route('eyesight.create',['id' => $patient->id]) }}" class="btn btn-sm btn-primary">New Record</a>
                            @else
                            <a href="{{ route('eyesight.edit',['id' => $patient->id]) }}" class="btn btn-sm btn-primary text-light">Edit</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            A Total Patients of {{ $patients->count() }}
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
    $('#myTable').DataTable();
</script>
@endpush
@push('styles')
<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush