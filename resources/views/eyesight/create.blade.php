@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card card-info mt-3">
		<div class="card-header">Eye Sight - Patient {{ $patient->firstname }} {{ $patient->lastname }}</div>
		<div class="card-body">
			<form method="POST" action="{{ route('eyesight.store',['id' => $id]) }}">
				@include('shared.alerts')
				<div class="row">
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="uc_fv_od">Uncorrected Far Vision OD:</label>
					    <input type="text" class="form-control" id="uc_fv_od" name="uc_fv_od" value="{{ old('uc_fv_od') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="uc_fv_os">Uncorrected Far Vision OS:</label>
					    <input type="text" class="form-control" id="uc_fv_os" name="uc_fv_os" value="{{ old('uc_fv_os') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="c_fv_od">Corrected Far Vision OD:</label>
					    <input type="text" class="form-control" id="c_fv_od" name="c_fv_od" value="{{ old('c_fv_od') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="c_fv_os">Corrected Far Vision OS:</label>
					    <input type="text" class="form-control" id="c_fv_os" name="c_fv_os" value="{{ old('c_fv_os') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="uc_nv_od">Uncorrected Near Vision OD:</label>
					    <input type="text" class="form-control" id="uc_nv_od" name="uc_nv_od" value="{{ old('uc_nv_od') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="uc_nv_os">Uncorrected Near Vision OS:</label>
					    <input type="text" class="form-control" id="uc_nv_os" name="uc_nv_os" value="{{ old('uc_nv_os') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="c_nv_od">Corrected Near Vision OD:</label>
					    <input type="text" class="form-control" id="c_nv_od" name="c_nv_od" value="{{ old('c_nv_od') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="c_nv_os">Corrected Near Vision OS:</label>
					    <input type="text" class="form-control" id="c_nv_os" name="c_nv_os" value="{{ old('c_nv_os') }}">
					</div>
					<div class="form-group col-12 col-sm-3">
					    <label class="form-control-label" for="optical">Optical:</label>
					   	<select class="form-control" name="optical" id="optical">
					   		<option value="0">For Opto Clearance</option>
					   		<option value="1">Cleared</option>
					   	</select>
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="colorvision">Color Vision:</label>
					    <select class="form-control" name="colorvision" id="colorvision">
					   		<option value="0">Defective</option>
					   		<option value="1">Adequate</option>
					   	</select>
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="ad">Hearing AD:</label>
					    <input type="text" class="form-control" id="ad" name="ad" value="{{ old('ad') }}">
					</div>
					<div class="form-group col-12 col-sm-2">
					    <label class="form-control-label" for="as">Hearing AS:</label>
					    <input type="text" class="form-control" id="as" name="as" value="{{ old('as') }}">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-12 col-sm-2">
						{{ csrf_field() }}
					    <button class="btn btn-info">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection