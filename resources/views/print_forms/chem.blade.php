@extends('layouts.master')

@section('title')
    Chem Form
@endsection

@section('content')
    <div class="container">
        <div class="col-md-12">
            <h1 class="text-center">ST. RITA MEDICAL & DIAGNOSTIC CENTER</h1>
            <h4 class="text-center">1705-B Dian St. cor. Finlandia Sts. Brgy. San Isidro, Makati City</h4>
            <h4 class="text-center">Tel.Nos. 845-1481/ 845-5652/ 845-5653 Fax: 886-5652</h4>
        </div>
        <div class="col-md-12">
            <h5 class="text-center mt-3">Accredited by POEA, DOH, DOLE, Marina, PCAH <br>and Malaysian Embassy</h5>
        </div>
        
    </div>
@endsection