<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VitalSign;
use App\Models\BasicInformation;
use App\Models\ActivityLog;
use Auth;                                                                                                                    
class VitalSignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vitalsign.index')
                ->with(['patients' => BasicInformation::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $patient = BasicInformation::find($id);
        return view('vitalsign.create')
                ->with([
                    'id' => $id,
                    'patient' => $patient
                    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request->validate([
            'height' => 'required',
            'weight' => 'required',
            'bp'     => 'required',
            'pulse' => 'required',
            'respiration' => 'required',
            'bmi' => 'required',
            'ppd' => 'required',
            'mmr' => 'required',
            'bodybuilt' => 'required',
            'lmp' => 'nullable',
            'lsc' => 'nullable',
            'method' => 'nullable',
        ]);
        $patient = new Vitalsign;
        $patient->information_id = $id;
        $patient->height = $request->height;
        $patient->weight = $request->weight;
        $patient->bp = $request->bp;
        $patient->pulse = $request->pulse;
        $patient->respiration = $request->respiration;
        $patient->bmi = $request->bmi;
        $patient->ppd = $request->ppd;
        $patient->lmp = $request->lmp;
        $patient->lsc = $request->lsc;
        $patient->bodybuilt = $request->bodybuilt;
        $patient->mmr = $request->mmr;
        $patient->method = $request->method;
        $patient->save();
        ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Created a vital sign information for patient '.$id]);
        return redirect()->route('vitalsign.index')->with('status','Successfully added a vital sign to patient '.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vitalsign = Vitalsign::where('information_id',$id)->first();
        $information = BasicInformation::find($vitalsign->information_id);
        return view('vitalsign.edit')
                ->with([
                    'vitals' => $vitalsign,
                    'patient' => $information
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'height' => 'required',
            'weight' => 'required',
            'bp'     => 'required',
            'pulse' => 'required',
            'respiration' => 'required',
            'bmi' => 'required',
            'ppd' => 'required',
            'mmr' => 'required',
            'bodybuilt' => 'required',
            'lmp' => 'nullable',
            'lsc' => 'nullable',
            'method' => 'nullable',
        ]);
        $patient = VitalSign::find($id);
        $patient->information_id = $id;
        $patient->height = $request->height;
        $patient->weight = $request->weight;
        $patient->bp = $request->bp;
        $patient->pulse = $request->pulse;
        $patient->respiration = $request->respiration;
        $patient->bmi = $request->bmi;
        $patient->ppd = $request->ppd;
        $patient->lmp = $request->lmp;
        $patient->lsc = $request->lsc;
        $patient->bodybuilt = $request->bodybuilt;
        $patient->mmr = $request->mmr;
        $patient->method = $request->method;
        $patient->save();
        ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Updated a vital sign information for patient '.$id]);
        return redirect()->route('vitalsign.index')->with('status','Successfully updated a vital sign to patient '.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
