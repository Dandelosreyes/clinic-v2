<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BasicInformation;
use App\Models\ActivityLog;
use App\Models\EyeSight;
use Auth;
class EyeSightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('eyesight.index')
                ->with(['patients' => BasicInformation::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('eyesight.create')
                ->with([
                    'id' => $id,
                    'patient' => BasicInformation::find($id)
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $request->validate([
            'uc_fv_od' => 'required|integer',
            'uc_fv_os' => 'required|integer',
            'c_fv_od' => 'required|integer',
            'c_fv_os' => 'required|integer',
            'uc_nv_od' => 'required|integer',
            'uc_nv_os' => 'required|integer',
            'c_nv_od' => 'required|integer',
            'c_nv_os' => 'required|integer',
            'optical' => 'required|boolean',
            'colorvision' => 'required|boolean',
            'ad' => 'required|integer',
            'as' => 'required|integer'
        ]);
        $patient = new EyeSight;
        $patient->information_id = $id;
        $patient->uncorrected_fv_od = $request->uc_fv_od;
        $patient->unccorected_fv_os = $request->uc_fv_os;
        $patient->uncorrected_nv_od = $request->uc_nv_od;
        $patient->unccorected_nv_os = $request->uc_nv_os;
        $patient->corrected_fv_od = $request->c_fv_od;
        $patient->corrected_fv_os = $request->c_fv_os;
        $patient->corrected_nv_od = $request->c_nv_od;
        $patient->corrected_nv_os = $request->c_nv_os;
        $patient->optical = $request->optical;
        $patient->colorvision = $request->colorvision;
        $patient->as = $request->as;
        $patient->ad = $request->ad;
        $patient->save();
        ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Created a vital sign information for patient '.$id]);
        return redirect()->route('eyesight.index')->with('status','Successfully added a vital sign to patient '.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = EyeSight::where('information_id',$id)->first();

        return view('eyesight.edit')
                ->with(['vitals' => $patient]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            
            'uc_fv_od' => 'required|integer',
            'uc_fv_os' => 'required|integer',
            'c_fv_od' => 'required|integer',
            'c_fv_os' => 'required|integer',
            'uc_nv_od' => 'required|integer',
            'uc_nv_os' => 'required|integer',
            'c_nv_od' => 'required|integer',
            'c_nv_os' => 'required|integer',
            'optical' => 'required|boolean',
            'colorvision' => 'required|boolean',
            'ad' => 'required|integer',
            'as' => 'required|integer'
        ]);
        $vitalsign = EyeSight::find($id);
        $vitalsign->uncorrected_fv_od = $request->uc_fv_od;
        $vitalsign->unccorected_fv_os = $request->uc_fv_os;
        $vitalsign->uncorrected_nv_od = $request->uc_nv_od;
        $vitalsign->unccorected_nv_os = $request->uc_nv_os;
        $vitalsign->corrected_fv_od = $request->c_fv_od;
        $vitalsign->corrected_fv_os = $request->c_fv_os;
        $vitalsign->corrected_nv_od = $request->c_nv_od;
        $vitalsign->corrected_nv_os = $request->c_nv_os;
        $vitalsign->optical = $request->optical;
        $vitalsign->colorvision = $request->colorvision;
        $vitalsign->as = $request->as;
        $vitalsign->ad = $request->ad;
        $vitalsign->save();
        ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Updated a vital sign information for patient '.$id]);
        return redirect()->route('eyesight.index')->with('status','Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
