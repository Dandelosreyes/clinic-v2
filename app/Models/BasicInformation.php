<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BasicInformation extends Model
{
    public function vitalsign()
    {
        return $this->hasMany('App\Models\VitalSign','information_id');
    }
    public function eyesight()
    {
        return $this->hasMany('App\Models\EyeSight','information_id');
    }
}
