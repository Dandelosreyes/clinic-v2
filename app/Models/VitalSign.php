<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VitalSign extends Model
{
    public function patient()
    {
        return $this->belongsTo('App\Models\BasicInformation','information_id','id');
    }
}
