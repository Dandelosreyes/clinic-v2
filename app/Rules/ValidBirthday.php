<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidBirthday implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(preg_match('/^[0-12],$/' , $value))
        {
            return true;
        }else
        {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The format must be mm/dd/yyyy';
    }
}
