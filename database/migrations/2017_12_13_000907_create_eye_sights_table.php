<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEyeSightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eye_sights', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('information_id')->unique();
            $table->foreign('information_id')->references('id')->on('basic_informations')->onDelete('cascade');
            $table->double('uncorrected_fv_od');
            $table->double('unccorected_fv_os');
            $table->double('corrected_fv_od');
            $table->double('corrected_fv_os'); 
            $table->double('uncorrected_nv_od');
            $table->double('unccorected_nv_os');
            $table->double('corrected_nv_od');
            $table->double('corrected_nv_os'); 
            $table->integer('optical');
            $table->integer('colorvision');
            $table->double('as');
            $table->double('ad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eye_sights');
    }
}
