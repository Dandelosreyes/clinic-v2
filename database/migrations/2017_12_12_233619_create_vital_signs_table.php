<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVitalSignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vital_signs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('information_id')->unique();
            $table->foreign('information_id')->references('id')->on('basic_informations')->onDelete('cascade');
            $table->string('height',10);
            $table->string('weight',15);
            $table->string('bp',15);
            $table->string('pulse',15);
            $table->string('respiration',15);
            $table->string('bmi',15);
            $table->string('ppd',15);
            $table->string('lmp',15)->nullable();
            $table->string('lsc',15)->nullable();
            $table->string('method',15)->nullable();
            $table->string('mmr');
            $table->string('bodybuilt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vital_signs');
    }
}
