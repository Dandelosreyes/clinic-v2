<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;
use App\Models\BasicInformation;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        User::create([ 
            'role_id' => 1, 
            'name' => 'Administrator', 
            'email' => 'admin@example.com' , 
            'licensenumber' => $faker->ean13, 
            'password' => '123']);
        User::create([ 
            'role_id' => 2, 
            'name' => 'Nurse 1', 
            'email' => 'nurse1@example.com' , 
            'licensenumber' => $faker->ean13, 
            'password' => '123']);
        User::create([ 
            'role_id' => 3, 
            'name' => 'Nurse 2', 
            'email' => 'nurse2@example.com' , 
            'licensenumber' => $faker->ean13, 
            'password' => '123']);
        User::create([ 
            'role_id' => 9, 
            'name' => 'Doc Eye', 
            'email' => 'de@example.com' , 
            'licensenumber' => $faker->ean13, 
            'password' => '123']);
        User::create([
            'role_id' => 10,
            'name' => 'Doc Utak',
            'email' => 'dp@example.com',
            'licensenumber' => $faker->ean13,
            'password' => '123'
        ]);
        BasicInformation::create([
            'lastname' => 'Delos Reyes',
            'firstname' => 'Daniel',
            'middlename' => 'Borreo',
            'mailingaddress' => 'Sample mailing address',
            'passport' => '5050XA',
            'gender' => 1,
            'birthday' => '02/07/1997',
            'telnumber' => '+6391234444',
            'occupation' => 'Sample Job',
            'destination' => 'Foreign',
            'placeofbirth' => 'Sample Place of birth',
            'history' => '1,2,3,4,5',
            'civilstatus' => 'Single'
        ]);
    }
}
