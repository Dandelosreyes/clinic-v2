<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class DiseasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('diseases')->insert([
        	[
	 			'disease' => 'Nose or throat trouble',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Ear trouble or deafness',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Asthma',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Tuberculosis',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Other lung diseases',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'High blood pressure',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Heart trouble',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Rheumatic Fever',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Diabetes Mellitus',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Endocrine Disorders',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Cancer or tumor',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Mental disorders',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Head or neck injury',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Hermia (ruptured)',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Rheumatism joint or back trouble',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Typhoid or paratyphoid fever',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Trachoma or other eye trouble',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Stomach pain or ulcer',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Other abdominal trouble',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Kidney or bladder trouble',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Sexually transmitted disease',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Genetic or famillial disoders',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Operations',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Tropical Diseases',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Chronic cough',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Fainting spells, fits or seizures',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Frequent headaches',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Dizzines',
	 			'created_at' => Carbon::now()
	 		],
	 		[
	 			'disease' => 'Malaria',
	 			'created_at' => Carbon::now()
	 		]
        ]);
    }
}
