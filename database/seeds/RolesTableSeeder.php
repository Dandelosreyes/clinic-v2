<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([ 'name' => 'Administrator' ]);
        Role::create([ 'name' => 'Nurse 1' ]);
        Role::create([ 'name' => 'Nurse 2' ]);
        Role::create([ 'name' => 'Radiologist' ]);
        Role::create([ 'name' => 'Lab Tech 1' ]);
        Role::create([ 'name' => 'Lab Tech 2' ]);
        Role::create([ 'name' => 'Lab Tech 3' ]);
        Role::create([ 'name' => 'Dentist' ]);
        Role::create([ 'name' => 'Optometrist' ]);
        Role::create([ 'name' => 'Psychologist' ]);
        Role::create([ 'name' => 'General Doctor' ]);
    }
}
