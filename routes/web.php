<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::view('/','print_forms.chem');
// Route::redirect('/','login');
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('test',function(){
	return response()->json(['test'=>'test']);	
});

Auth::routes();
Route::resource('information','BasicInformationController')->except(['delete']);
Route::name('vitalsign.')->prefix('vitalsign/')->group(function(){
    Route::get('/','VitalSignController@index')->name('index');
	Route::get('create/{id}','VitalSignController@create')->name('create');
	Route::post('store/{id}','VitalSignController@store')->name('store');
	Route::get('edit/{id}','VitalSignController@edit')->name('edit');
	Route::patch('update/{id}','VitalSignController@update')->name('update');
});
Route::name('eyesight.')->prefix('eyesight/')->group(function(){
    Route::get('/','EyeSightController@index')->name('index');
	Route::get('create/{id}','EyeSightController@create')->name('create');
	Route::post('store/{id}','EyeSightController@store')->name('store');
	Route::get('edit/{id}','EyeSightController@edit')->name('edit');
	Route::patch('update/{id}','EyeSightController@update')->name('update');
});
Route::name('psychology.')->prefix('psychology/')->group(function(){
    Route::get('/','EyeSightController@index')->name('index');
	Route::get('create/{id}','EyeSightController@create')->name('create');
	Route::post('store/{id}','EyeSightController@store')->name('store');
	Route::get('edit/{id}','EyeSightController@edit')->name('edit');
	Route::patch('update/{id}','EyeSightController@update')->name('update');
});
Route::get('/home', 'HomeController@index')->name('home');

